BEGIN;
    ALTER TABLE thread ADD message_count INTEGER NOT NULL DEFAULT 0;
    ALTER TABLE thread ADD CONSTRAINT message_count_positive CHECK(message_count >= 0);

    UPDATE thread SET message_count = messages.num_messages FROM (
        SELECT thread_id, COUNT(*) AS num_messages FROM thread_message GROUP BY thread_id
    ) AS messages
    WHERE thread.id = messages.thread_id;
COMMIT;
