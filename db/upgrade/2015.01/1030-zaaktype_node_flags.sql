BEGIN;
    ALTER TABLE zaaktype_node DROP IF EXISTS contact_info_email_required;
    ALTER TABLE zaaktype_node DROP IF EXISTS contact_info_phone_required;
    ALTER TABLE zaaktype_node DROP IF EXISTS contact_info_mobile_phone_required;

    ALTER TABLE zaaktype_node ADD contact_info_email_required BOOLEAN DEFAULT FALSE;
    ALTER TABLE zaaktype_node ADD contact_info_phone_required BOOLEAN DEFAULT FALSE;
    ALTER TABLE zaaktype_node ADD contact_info_mobile_phone_required BOOLEAN DEFAULT FALSE;

    -- Backwards compatibility
    UPDATE zaaktype_node SET contact_info_email_required = true;
COMMIT;
