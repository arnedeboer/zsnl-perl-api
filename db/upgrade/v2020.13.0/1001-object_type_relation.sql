BEGIN;

ALTER TABLE custom_object_version
    ADD CONSTRAINT custom_object_version_key UNIQUE (id, custom_object_id);

-- DROP TABLE custom_object_case_relation;
CREATE TABLE custom_object_case_relation (
    custom_object_id integer NOT NULL,
    case_id integer NOT NULL,
    custom_object_version_id integer NOT NULL,
    PRIMARY KEY (custom_object_id, case_id),
    FOREIGN KEY (case_id) REFERENCES zaak (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (custom_object_id, custom_object_version_id) REFERENCES custom_object_version (custom_object_id, id) ON DELETE CASCADE ON UPDATE CASCADE
);

COMMIT;

