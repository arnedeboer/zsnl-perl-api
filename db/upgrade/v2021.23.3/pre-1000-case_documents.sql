
BEGIN;

  DROP TABLE IF EXISTS case_documents_cache CASCADE;

  CREATE TABLE case_documents_cache (
      case_id integer NOT NULL,
      value text[],
      value_v0 jsonb default '[]'::jsonb not null,
      value_v1 jsonb default '[]'::jsonb not null,
      magic_string text not null,
      library_id integer not null,
      PRIMARY KEY (case_id, library_id)
  );

  CREATE OR REPLACE FUNCTION set_view_values_for_file()
      RETURNS TRIGGER
      LANGUAGE plpgsql
  AS $$
  BEGIN

    SELECT INTO NEW.value_v1 attribute_value_to_jsonb(NEW.value, 'file');
    SELECT INTO NEW.value_v0 attribute_value_to_v0(NEW.value, 'file', true);

    RETURN NEW;
  END
  $$;

  CREATE TRIGGER set_view_values_for_file
    BEFORE INSERT
    OR UPDATE
    ON case_documents_cache
    FOR EACH ROW
    EXECUTE PROCEDURE set_view_values_for_file();

  CREATE OR REPLACE FUNCTION create_document_cache_for_case(
    IN id int
  ) RETURNS void
  LANGUAGE plpgsql
  AS $$
  BEGIN
      INSERT INTO case_documents_cache (case_id, value, magic_string, library_id)
        SELECT case_id, value, magic_string, library_id FROM case_documents
          WHERE case_id = id;
  END;
  $$;

  CREATE OR REPLACE FUNCTION initialize_case_document_cache_for_case ()
      RETURNS TRIGGER
      LANGUAGE plpgsql
  AS $$
  BEGIN
    DELETE FROM case_documents_cache WHERE case_id = NEW.id;
    PERFORM create_document_cache_for_case(NEW.id);
    RETURN NULL;
  END
  $$;

  DROP TRIGGER IF EXISTS initialize_case_document_cache_for_case ON "zaak";
  CREATE TRIGGER initialize_case_document_cache_for_case
     AFTER INSERT
     ON zaak
     FOR EACH ROW
     EXECUTE PROCEDURE initialize_case_document_cache_for_case();

  DROP TRIGGER IF EXISTS reset_case_document_cache_for_case ON "zaak";
  CREATE TRIGGER reset_case_document_cache_for_case
     AFTER UPDATE OF zaaktype_node_id
     ON zaak
     FOR EACH ROW
     EXECUTE PROCEDURE initialize_case_document_cache_for_case();

  CREATE OR REPLACE FUNCTION update_document_cache_for_case(
    IN id int,
    IN lib_id int
  ) RETURNS void
  LANGUAGE plpgsql
  AS $$
  BEGIN
    UPDATE case_documents_cache me
      SET value = subquery.value
      FROM ( SELECT * FROM case_documents WHERE case_id = id AND library_id = lib_id) as subquery
      WHERE me.case_id = id and me.library_id = lib_id;

  END;
  $$;

  CREATE OR REPLACE FUNCTION update_case_document_cache_for_case ()
      RETURNS TRIGGER
      LANGUAGE plpgsql
  AS $$
  BEGIN
    IF TG_OP = 'DELETE'
    THEN
      PERFORM update_document_cache_for_case(OLD.case_id, OLD.bibliotheek_kenmerken_id);
    ELSE
      PERFORM update_document_cache_for_case(NEW.case_id, NEW.bibliotheek_kenmerken_id);
    END IF;
    RETURN NULL;
  END
  $$;

  DROP TRIGGER IF EXISTS update_case_document_cache_for_case ON "file_case_document";
  CREATE TRIGGER update_case_document_cache_for_case
     AFTER INSERT OR UPDATE OR DELETE
     ON file_case_document
     FOR EACH ROW
     EXECUTE PROCEDURE update_case_document_cache_for_case();

COMMIT;
