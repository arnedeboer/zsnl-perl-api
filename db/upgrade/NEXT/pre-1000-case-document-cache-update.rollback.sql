
BEGIN;

 DROP FUNCTION IF EXISTS attribute_file_value_to_v0_jsonb;

  CREATE OR REPLACE FUNCTION attribute_file_value_to_v0_jsonb(
    IN value text[],
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    length int;
    m text;

    r record;

  BEGIN

    value_json := '[]'::jsonb;

    FOR r IN
      SELECT
        f.accepted as accepted,
        f.confidential as confidential,
        CONCAT(f.name, f.extension) as filename,
        f.id as id,
        fs.uuid as uuid,
        fs.size as filesize,
        fs.original_name as original_name,
        fs.mimetype as mimetype,
        fs.md5 as md5,
        fs.is_archivable as is_archivable
      FROM
        filestore fs
      JOIN
        file f
      ON
        (f.filestore_id = fs.id)
      WHERE
        fs.uuid = ANY(value::uuid[])
      ORDER by f.id
    LOOP

      select into value_json value_json || jsonb_build_object(
        'accepted', CASE WHEN r.accepted = true THEN 1 ELSE 0 END,
        'confidential', r.confidential,
        'file_id', r.id,
        'filename', r.filename,
        'is_archivable', CASE WHEN r.is_archivable = true THEN 1 ELSE 0 END,
        'md5', r.md5,
        'mimetype', r.mimetype,
        'original_name', r.original_name,
        'size', r.filesize,
        'thumbnail_uuid', null,
        'uuid', r.uuid
      );
    END LOOP;

    RETURN;


  END;
  $$;

  CREATE OR REPLACE FUNCTION set_view_values_for_file()
      RETURNS TRIGGER
      LANGUAGE plpgsql
  AS $$
  BEGIN

    SELECT INTO NEW.value_v1 attribute_value_to_jsonb(NEW.value, 'file');
    SELECT INTO NEW.value_v0 attribute_value_to_v0(NEW.value, 'file', true);

    RETURN NEW;
  END
  $$;

COMMIT;
