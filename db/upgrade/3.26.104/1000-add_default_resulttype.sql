BEGIN;
    /* ZS-16930__default_resultype.sql */

    ALTER TABLE zaaktype_resultaten
        ADD COLUMN IF NOT EXISTS standaard_keuze
            BOOLEAN
            NOT NULL
            DEFAULT FALSE;

COMMIT;
