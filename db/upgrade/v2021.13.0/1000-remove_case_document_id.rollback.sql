BEGIN;

    ALTER TABLE file_case_document ADD COLUMN case_document_id INTEGER;

    UPDATE
        file_case_document
    SET
        case_document_id = zdkm.case_document_id
    FROM
        zaaktype_document_kenmerken_map zdkm,
        zaak z,
        file f
    WHERE
        zdkm.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id
        AND zdkm.zaaktype_node_id = z.zaaktype_node_id
        AND z.id = f.case_id
        AND f.id = file_case_document.file_id;

    ALTER TABLE file_case_document ALTER case_document_id SET NOT NULL;
    ALTER TABLE file_case_document ADD CONSTRAINT case_document_id_fk FOREIGN KEY(case_document_id) REFERENCES zaaktype_kenmerken(id);

COMMIT;
