BEGIN;

  DROP FUNCTION IF EXISTS get_date_progress_from_case(timestamp without time zone, timestamp without time zone, timestamp without time zone, text);

  CREATE OR REPLACE FUNCTION get_date_progress_from_case(
    zaak hstore
  )
  RETURNS text
  IMMUTABLE
  LANGUAGE plpgsql
  AS $$
  DECLARE
    completion_date timestamp with time zone;
    completion_epoch bigint;
    start_epoch bigint;
    target_epoch bigint;

    current_difference bigint;
    max_difference bigint;

    perc bigint;
  BEGIN

    IF zaak->'status' = 'stalled'
    THEN
      RETURN '';
    END IF;

    IF zaak->'afhandeldatum' IS NOT NULL
    THEN
      completion_date := (zaak->'afhandeldatum')::timestamp without time zone;
    ELSE
      SELECT INTO completion_date NOW()::timestamp without time zone;
    END IF;

    SELECT INTO completion_epoch date_part('epoch', completion_date);
    SELECT INTO start_epoch date_part('epoch', (zaak->'registratiedatum')::timestamp without time zone);

    current_difference := completion_epoch - start_epoch;

    SELECT INTO target_epoch date_part('epoch', (zaak->'streefafhandeldatum')::timestamp without time zone);

    max_difference := target_epoch - start_epoch;
    max_difference := GREATEST(max_difference, 1);

    perc := ROUND(100 * ( current_difference::numeric / max_difference::numeric ));

    RETURN perc::text;
  END;
  $$;

COMMIT;
