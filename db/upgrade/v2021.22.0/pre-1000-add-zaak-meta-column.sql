BEGIN;
  ALTER TABLE zaak_meta ADD COLUMN relations_complete BOOLEAN DEFAULT true NOT NULL;

  UPDATE zaak_meta SET relations_complete = false WHERE zaak_id IN (
    SELECT DISTINCT(pid) FROM zaak WHERE pid IS NOT NULL AND status != 'resolved'
  );
COMMIT;
