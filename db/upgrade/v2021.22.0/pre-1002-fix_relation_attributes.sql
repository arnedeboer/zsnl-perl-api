BEGIN;

CREATE OR REPLACE FUNCTION transform_relation_attribute_value(old_value JSONB)
    RETURNS varchar
    AS $$
DECLARE
    new_value JSONB;
BEGIN

    IF old_value ? 'link' THEN
        new_value = jsonb_build_object(
            'type', 'relationship',
            'value', COALESCE(old_value->>'id', old_value->>'name'),
            'specifics', jsonb_build_object(
                'relationship_type', old_value->>'type',
                'metadata', jsonb_build_object(
                    'summary', old_value->>'label'
                )
            )
        );
    ELSE
        new_value = old_value;
    END IF;

    RETURN new_value::VARCHAR;
END;
$$
LANGUAGE plpgsql
IMMUTABLE PARALLEL SAFE;

-- Make a backup, so we do not lose any data:
CREATE TABLE backup_minty8010_zaaktype_kenmerk AS (
    SELECT
        zk.id,
        nr,
        elem
    FROM
        zaak_kenmerk zk,
        bibliotheek_kenmerken bk,
        unnest(zk.value)
        WITH ORDINALITY AS a (elem, nr)
    WHERE
        bk.id = zk.bibliotheek_kenmerken_id
        AND bk.value_type = 'relationship');
CREATE UNIQUE INDEX ON backup_minty8010_zaaktype_kenmerk(id, nr);

WITH kenmerk_values AS NOT MATERIALIZED (
    SELECT
        id,
        nr,
        elem
    FROM
        backup_minty8010_zaaktype_kenmerk
)
UPDATE
    zaak_kenmerk
SET
    value = ARRAY (
        SELECT
            transform_relation_attribute_value (kv.elem::JSONB)
        FROM
            kenmerk_values kv
        WHERE
            kv.id = zaak_kenmerk.id
        ORDER BY
            kv.nr)
FROM kenmerk_values AS kv
WHERE zaak_kenmerk.id = kv.id;

DROP FUNCTION transform_relation_attribute_value;

COMMIT;