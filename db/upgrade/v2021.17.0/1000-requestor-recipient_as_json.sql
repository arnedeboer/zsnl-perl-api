BEGIN;

  ALTER TABLE natuurlijk_persoon ADD COLUMN surname text GENERATED ALWAYS AS (
    COALESCE(naamgebruik, geslachtsnaam)
  ) STORED;

  ALTER TABLE gm_natuurlijk_persoon ADD COLUMN surname text GENERATED ALWAYS AS (
    COALESCE(naamgebruik, geslachtsnaam)
  ) STORED;

COMMIT;

BEGIN;

  DROP FUNCTION IF EXISTS complete_house_number(text,text,text);

  CREATE OR REPLACE FUNCTION complete_house_number(
    IN house_number text,
    IN letter text,
    IN postfix text,
    OUT complete text
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE

  BEGIN

    IF house_number IS NULL
    THEN
      complete = '';
      RETURN;
    END IF;

    complete := house_number::text;

    IF letter IS NOT NULL AND length(letter) > 0
    THEN
      complete := CONCAT(complete, ' ', letter);
    END IF;


    IF postfix IS NOT NULL AND length(postfix) > 0
    THEN
      complete := CONCAT(complete, ' - ', postfix);
    END IF;

    RETURN;

  END;
  $$;

  CREATE OR REPLACE FUNCTION empty_subject_as_v0_json(
    IN type text,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    key text;

    field text;
    fields text[];

  BEGIN

    key := CONCAT('case.', type, '.');

    fields := ARRAY[
      'a_number',
      'bsn',
      'coc',
      'correspondence_house_number',
      'correspondence_place_of_residence',
      'correspondence_street',
      'correspondence_zipcode',
      'country_of_birth',
      'country_of_residence',
      'date_of_birth',
      'date_of_death',
      'date_of_divorce',
      'date_of_marriage',
      'department',
      'display_name',
      'email',
      'establishment_number',
      'family_name',
      'first_names',
      'foreign_residence_address_line1',
      'foreign_residence_address_line2',
      'foreign_residence_address_line3',
      'full_name',
      'gender',
      'has_correspondence_address',
      'house_number',
      'id',
      'initials',
      'investigation',
      'is_secret',
      'login',
      'mobile_number',
      'naamgebruik',
      'name',
      'noble_title',
      'password',
      'phone_number',
      'place_of_birth',
      'place_of_residence',
      'preferred_contact_channel',
      'residence_house_number',
      'residence_place_of_residence',
      'residence_street',
      'residence_zipcode',
      'salutation',
      'salutation1',
      'salutation2',
      'status',
      'street',
      'subject_type',
      'surname',
      'surname_prefix',
      'title',
      'trade_name',
      'type',
      'type_of_business_entity',
      'uuid',
      'zipcode'
    ];

    json := '{}'::jsonb;
    FOREACH field IN ARRAY fields
    LOOP
      SELECT INTO json json || jsonb_build_object(
        CONCAT(key, field), null
      );
    END LOOP;

  END;
  $$;

  CREATE OR REPLACE FUNCTION employee_as_v0_json(
    IN subject hstore,
    IN type text,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    key text;

    properties jsonb;
    config_row jsonb;

    ue record;
    is_active boolean;

  BEGIN

    key := CONCAT('case.', type, '.');

    properties := (subject->'properties')::jsonb;

    SELECT INTO config_row jsonb_object_agg(config.parameter, config.value) from config where parameter like 'customer_info%';

    is_active := false;
    FOR ue IN
      select distinct(active) as active from user_entity where subject_id = (subject->'id')::int and date_deleted is null
    LOOP
      IF ue.active = true THEN
        is_active := true;
        EXIT;
      END IF;
    END LOOP;

    SELECT INTO json jsonb_build_object(

      type, subject->'uuid',
      CONCAT(key, 'type'), 'Behandelaar',
      CONCAT(key, 'subject_type'), 'medewerker',

      CONCAT(key, 'status'), CASE WHEN is_active = true THEN 'Actief' ELSE 'Inactief' END,

      CONCAT(key, 'uuid'), subject->'uuid',
      CONCAT(key, 'id'), CONCAT('betrokkene-medewerker-', subject->'id'),
      CONCAT(key, 'preferred_contact_channel'), null,
      CONCAT(key, 'country_of_residence'), null,

      CONCAT(key, 'display_name'), properties->'displayname',
      CONCAT(key, 'name'), properties->'displayname',
      CONCAT(key, 'full_name'), properties->'displayname',
      CONCAT(key, 'initials'), properties->'initials',
      CONCAT(key, 'naamgebruik'), properties->'sn',
      CONCAT(key, 'surname'), properties->'sn',

      CONCAT(key, 'department'), subject->'department',

      CONCAT(key, 'email'), properties->'mail',
      CONCAT(key, 'phone_number'), properties->'telephonenumber',

      -- blub
      CONCAT(key, 'mobile_number'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'password'), null,

      -- In use...
      CONCAT(key, 'correspondence_house_number'), config_row->'customer_info_huisnummer',
      CONCAT(key, 'correspondence_place_of_residence'), config_row->'customer_info_woonplaats',
      CONCAT(key, 'correspondence_street'), config_row->'customer_info_straatnaam',
      CONCAT(key, 'correspondence_zipcode'), config_row->'customer_info_postcode',

      CONCAT(key, 'residence_house_number'), config_row->'customer_info_huisnummer',
      CONCAT(key, 'residence_place_of_residence'), config_row->'customer_info_woonplaats',
      CONCAT(key, 'residence_street'), config_row->'customer_info_straatnaam',
      CONCAT(key, 'residence_zipcode'), config_row->'customer_info_postcode',

      CONCAT(key, 'house_number'), config_row->'customer_info_huisnummer',
      CONCAT(key, 'place_of_residence'), config_row->'customer_info_woonplaats',
      CONCAT(key, 'street'), config_row->'customer_info_straatnaam',
      CONCAT(key, 'zipcode'), config_row->'customer_info_postcode'

    ) -- work around >100 keys
    || jsonb_build_object(
      -- Not relevant for an employee
      CONCAT(key, 'coc'), null,
      CONCAT(key, 'establishment_number'), null,
      CONCAT(key, 'type_of_business_entity'), null,
      CONCAT(key, 'trade_name'), null,
      CONCAT(key, 'has_correspondence_address'), null,
      CONCAT(key, 'foreign_residence_address_line1'), null,
      CONCAT(key, 'foreign_residence_address_line2'), null,
      CONCAT(key, 'foreign_residence_address_line3'), null,
      CONCAT(key, 'investigation'), null,
      CONCAT(key, 'a_number'), null,
      CONCAT(key, 'bsn'), null,
      CONCAT(key, 'family_name'), null,
      CONCAT(key, 'first_names'), null,
      CONCAT(key, 'gender'), null,
      CONCAT(key, 'salutation'), null,
      CONCAT(key, 'salutation1'), null,
      CONCAT(key, 'salutation2'), null,
      CONCAT(key, 'is_secret'), null,
      CONCAT(key, 'naamgebruik'), null,
      CONCAT(key, 'noble_title'), null,
      CONCAT(key, 'place_of_birth'), null,
      CONCAT(key, 'surname_prefix'), null,
      CONCAT(key, 'country_of_birth'), null,
      CONCAT(key, 'date_of_birth'), null,
      CONCAT(key, 'date_of_death'), null,
      CONCAT(key, 'date_of_divorce'), null,
      CONCAT(key, 'date_of_marriage'), null

    );

  END;
  $$;

  CREATE OR REPLACE FUNCTION company_as_v0_json(
    IN subject hstore,
    IN type text,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    key text;
    is_correspondence boolean;
    tmp text;

    active boolean;
    bid text;

  BEGIN

    key := CONCAT('case.', type, '.');

    is_correspondence := false;

    FOREACH tmp IN ARRAY ARRAY['woonplaats', 'straatnaam', 'postcode', 'adres_buitenland1', 'adres_buitenland2', 'adres_buitenland3']
    LOOP
      tmp := CONCAT('correspondentie_', tmp);
      IF subject->tmp IS NOT NULL AND LENGTH(subject->tmp) > 0 THEN
        is_correspondence := true;
        EXIT;
      END IF;
    END LOOP;


    SELECT INTO json jsonb_build_object(

      CONCAT(key, 'foreign_residence_address_line1'), subject->'vestiging_adres_buitenland1',
      CONCAT(key, 'foreign_residence_address_line2'), subject->'vestiging_adres_buitenland2',
      CONCAT(key, 'foreign_residence_address_line3'), subject->'vestiging_adres_buitenland3',

      CONCAT(key, 'correspondence_house_number'), complete_house_number(
        subject->'correspondentie_huisnummer', subject->'correspondentie_huisletter', subject->'correspondentie_huisnummertoevoeging'
      ),
      CONCAT(key, 'correspondence_place_of_residence'), subject->'correspondentie_woonplaats',
      CONCAT(key, 'correspondence_street'), subject->'correspondentie_straatnaam',
      CONCAT(key, 'correspondence_zipcode'), subject->'correspondentie_postcode',

      CONCAT(key, 'residence_house_number'), complete_house_number(
        subject->'vestiging_huisnummer', subject->'vestiging_huisletter', subject->'vestiging_huisnummertoevoeging'
      ),
      CONCAT(key, 'residence_place_of_residence'), subject->'vestiging_woonplaats',
      CONCAT(key, 'residence_street'), subject->'vestiging_straatnaam',
      CONCAT(key, 'residence_zipcode'), subject->'vestiging_postcode'

    );

    IF is_correspondence = true THEN
      SELECT INTO json json || jsonb_build_object(
        CONCAT(key, 'street'),  subject->'correspondentie_straatnaam',
        CONCAT(key, 'zipcode'), subject->'correspondentie_postcode',
        CONCAT(key, 'place_of_residence'), subject->'correspondentie_woonplaats',
        CONCAT(key, 'house_number'), complete_house_number(
          subject->'correspondentie_huisnummer', subject->'correspondentie_huisletter', subject->'correspondentie_huisnummertoevoeging'
        )
      );
    ELSE
      SELECT INTO json json || jsonb_build_object(
        CONCAT(key, 'street'),  subject->'vestiging_straatnaam',
        CONCAT(key, 'zipcode'), subject->'vestiging_postcode',
        CONCAT(key, 'place_of_residence'), subject->'vestiging_woonplaats',
        CONCAT(key, 'house_number'), complete_house_number(
          subject->'vestiging_huisnummer', subject->'vestiging_huisletter', subject->'vestiging_huisnummertoevoeging'
        )
      );
    END IF;

    -- active does not exists in the gm_ table of bedrijf
    -- and is always active, since well, you can't create cases with
    -- inactive people
    active := true;

    IF subject->'active' IS NOT NULL
    THEN
      active := (subject->'active')::boolean;
    END IF;

    IF subject->'gegevens_magazijn_id' IS NOT NULL
    THEN
      bid := CONCAT('betrokkene-bedrijf-', subject->'gegevens_magazijn_id');
    ELSE
      bid := CONCAT('betrokkene-bedrijf-', subject->'id');
    END IF;

    SELECT INTO json json || jsonb_build_object(
      type, subject->'uuid',
      CONCAT(key, 'type'), 'Niet natuurlijk persoon',
      CONCAT(key, 'subject_type'), 'bedrijf',

      CONCAT(key, 'status'), CASE WHEN active = true THEN 'Actief' ELSE 'Inactief' END,

      CONCAT(key, 'id'), bid,

      CONCAT(key, 'uuid'), subject->'uuid',
      CONCAT(key, 'preferred_contact_channel'), subject->'preferred_contact_channel',
      CONCAT(key, 'country_of_residence'), COALESCE(
        subject->'country_of_residence',
        CONCAT('Onbekende landcode: ', subject->'vestiging_landcode')
      ),

      CONCAT(key, 'name'), subject->'handelsnaam',
      CONCAT(key, 'display_name'), subject->'handelsnaam',
      CONCAT(key, 'coc'), subject->'dossiernummer',
      CONCAT(key, 'establishment_number'), subject->'vestigingsnummer',
      -- we have the actual data?
      CONCAT(key, 'type_of_business_entity'), (subject->'rechtsvorm')::int,
      CONCAT(key, 'trade_name'), subject->'handelsnaam',
      CONCAT(key, 'has_correspondence_address'), is_correspondence,

      CONCAT(key, 'email'), subject->'email_address',
      CONCAT(key, 'mobile_number'), subject->'mobile',
      CONCAT(key, 'phone_number'), subject->'phone_number',

      -- Not relevant for a company
      CONCAT(key, 'investigation'), null,
      CONCAT(key, 'a_number'), null,
      CONCAT(key, 'bsn'), null,
      CONCAT(key, 'family_name'), null,
      CONCAT(key, 'first_names'), null,
      CONCAT(key, 'surname'), null,
      CONCAT(key, 'full_name'), null,
      CONCAT(key, 'gender'), null,
      CONCAT(key, 'salutation'), null,
      CONCAT(key, 'salutation1'), null,
      CONCAT(key, 'salutation2'), null,
      CONCAT(key, 'initials'), null,
      CONCAT(key, 'is_secret'), null,
      CONCAT(key, 'naamgebruik'), null,
      CONCAT(key, 'noble_title'), null,
      CONCAT(key, 'place_of_birth'), null,
      CONCAT(key, 'surname_prefix'), null,
      CONCAT(key, 'country_of_birth'), null,
      CONCAT(key, 'date_of_birth'), null,
      CONCAT(key, 'date_of_death'), null,
      CONCAT(key, 'date_of_divorce'), null,
      CONCAT(key, 'date_of_marriage'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'department'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'password'), null

    );

  END;
  $$;


  CREATE OR REPLACE FUNCTION person_as_v0_json(
    IN np hstore,
    IN type text,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    key text;
    gender text;
    salutation text;
    salutation1 text;
    salutation2 text;

    address_inactive text;
    address_active text;
    house_number text;

    investigation boolean;
    active boolean;
    tmp text;
    bid text;

  BEGIN

    key := CONCAT('case.', type, '.');

    investigation := false;

    FOREACH tmp IN ARRAY ARRAY['persoon', 'huwelijk', 'overlijden', 'verblijfplaats']
    LOOP
      tmp := CONCAT('onderzoek_', tmp);
      IF (np->tmp)::boolean = true THEN
        investigation := true;
        EXIT;
      END IF;
    END LOOP;

    gender := LOWER(np->'geslachtsaanduiding');

    IF gender = 'm'
    THEN
      gender := 'man';
      salutation := 'meneer';
      salutation1 := 'heer';
      salutation2 := 'de heer';
    ELSIF gender = 'v'
    THEN
      gender := 'vrouw';
      salutation := 'mevrouw';
      salutation1 := 'heer';
      salutation2 := 'de heer';
    ELSE
      gender := '';
      salutation := '';
      salutation1 := '';
      salutation2 := '';
    END IF;

    house_number := complete_house_number(
      np->'street_number',
      np->'street_number_letter',
      np->'street_number_suffix'
    );

    SELECT INTO json jsonb_build_object(

      /* This is so wrong on so many levels:
       *
       * This needs to be NULL if the country_code = 6030 This can only
       * be used when address_type = 'W'. Yet, we allow 'B' and
       * incorrect data. For now, respect the old incorrect API.
       */
      CONCAT(key, 'foreign_residence_address_line1'), np->'foreign_address_line_1',
      CONCAT(key, 'foreign_residence_address_line2'), np->'foreign_address_line_2',
      CONCAT(key, 'foreign_residence_address_line3'), np->'foreign_address_line_3',

      /* This is also wrong.
       *
       * This needs to be NULL if the country_code != 6030
       */
      CONCAT(key, 'street'), np->'street',
      CONCAT(key, 'zipcode'), np->'zipcode',
      CONCAT(key, 'place_of_residence'), np->'city',
      CONCAT(key, 'house_number'), house_number
    );

    IF np->'address_type' = 'W' THEN
      address_active = 'residence';
      address_inactive = 'correspondence';
    ELSE
      address_inactive = 'residence';
      address_active = 'correspondence';
    END IF;

    SELECT INTO json json || jsonb_build_object(
      CONCAT(key, address_active, '_house_number'), house_number,
      CONCAT(key, address_active, '_place_of_residence'), np->'city',
      CONCAT(key, address_active, '_street'), np->'street',
      CONCAT(key, address_active, '_zipcode'), np->'zipcode',

      CONCAT(key, address_inactive, '_house_number'), '',
      CONCAT(key, address_inactive, '_place_of_residence'), null,
      CONCAT(key, address_inactive, '_street'), null,
      CONCAT(key, address_inactive, '_zipcode'), null
    );

    -- active does not exists in the gm_ table of NP
    -- and is always active, since well, you can't create cases with
    -- inactive people
    active := true;
    IF np->'active' IS NOT NULL
    THEN
      active := (np->'active')::boolean;
    END IF;

    IF np->'gegevens_magazijn_id' IS NOT NULL
    THEN
      bid := CONCAT('betrokkene-natuurlijk_persoon-', np->'gegevens_magazijn_id');
    ELSE
      bid := CONCAT('betrokkene-natuurlijk_persoon-', np->'id');
    END IF;

    SELECT INTO json json || jsonb_build_object(
      type, np->'uuid',
      CONCAT(key, 'type'), 'Natuurlijk persoon',
      CONCAT(key, 'subject_type'), 'natuurlijk_persoon',

      CONCAT(key, 'status'), CASE WHEN active = true THEN 'Actief' ELSE 'Inactief' END,
      CONCAT(key, 'id'), bid,

      CONCAT(key, 'uuid'), np->'uuid',
      CONCAT(key, 'a_number'), np->'a_nummer',
      CONCAT(key, 'bsn'), np->'burgerservicenummer',

      CONCAT(key, 'family_name'), np->'geslachtsnaam',
      CONCAT(key, 'first_names'), np->'voornamen',
      CONCAT(key, 'surname'), np->'naamgebruik',
      CONCAT(key, 'full_name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'display_name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'gender'), gender,
      CONCAT(key, 'salutation'), salutation,
      CONCAT(key, 'salutation1'), salutation1,
      CONCAT(key, 'salutation2'), salutation2,

      CONCAT(key, 'initials'), np->'voorletters',
      CONCAT(key, 'is_secret'), np->'indicatie_geheim',
      CONCAT(key, 'naamgebruik'), np->'naamgebruik',
      CONCAT(key, 'name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'noble_title'), np->'adellijke_titel',
      CONCAT(key, 'place_of_birth'), np->'geboorteplaats',
      CONCAT(key, 'preferred_contact_channel'), np->'preferred_contact_channel',

      CONCAT(key, 'investigation'), investigation,
      CONCAT(key, 'surname_prefix'), np->'voorvoegsel',

      CONCAT(key, 'country_of_birth'), np->'geboorteland',
      CONCAT(key, 'country_of_residence'), np->'country',
      CONCAT(key, 'date_of_birth'), (np->'geboortedatum')::date,
      CONCAT(key, 'date_of_death'), (np->'overlijdensdatum')::date,
      CONCAT(key, 'date_of_divorce'), (np->'datum_huwelijk_ontbinding')::date,
      CONCAT(key, 'date_of_marriage'), (np->'datum_huwelijk')::date,

      CONCAT(key, 'email'), np->'email_address',
      CONCAT(key, 'mobile_number'), np->'mobile',
      CONCAT(key, 'phone_number'), np->'phone_number',
      CONCAT(key, 'has_correspondence_address'), CASE WHEN np->'address_type' = 'W' THEN false ELSE true END,

      -- Not relevant for NP
      CONCAT(key, 'coc'), null,
      CONCAT(key, 'department'), null,
      CONCAT(key, 'establishment_number'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'trade_name'), null,
      CONCAT(key, 'type_of_business_entity'), null,
      CONCAT(key, 'password'), null
    );

  END;
  $$;

  CREATE OR REPLACE FUNCTION case_subject_as_v0_json(
    IN zb hstore,
    IN type text,
    IN historic boolean,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    btype text;

    gm_id int;
    b_id int;

    subject record;
    s_hstore hstore;
    subject_json json;

    contactdata_person int;
    contactdata_company int;

  BEGIN

    contactdata_person  := 1;
    contactdata_company := 2;

    btype := zb->'betrokkene_type';
    gm_id := (zb->'gegevens_magazijn_id')::int;
    b_id  := (zb->'betrokkene_id')::int;

    IF historic = true
    THEN
      type := CONCAT(type, '_snapshot');
    END IF;

    IF btype IS NULL THEN
      SELECT INTO json empty_subject_as_v0_json(type);
      RETURN;
    END IF;

    IF btype NOT IN ('medewerker', 'natuurlijk_persoon', 'bedrijf') THEN
      RAISE EXCEPTION 'Unknown betrokkene type %', btype;
    END IF;

    IF btype = 'medewerker' THEN

      -- There is no difference between historic data for employees

      SELECT INTO
        subject
        s.*,
        gr.name as department

      FROM
        subject s
      LEFT JOIN
        groups gr
      ON
        s.group_ids[1] = gr.id
      WHERE
        s.subject_type = 'employee'
      AND
        s.id = gm_id;

      s_hstore := hstore(subject);

      SELECT INTO json employee_as_v0_json(s_hstore, type);

    ELSIF btype = 'natuurlijk_persoon' THEN

      IF historic = false
      THEN

        SELECT INTO
          subject
          np.*,
          a.straatnaam as street,
          a.huisnummer as street_number,
          a.huisletter as street_number_letter,
          a.huisnummertoevoeging as street_number_suffix,
          a.postcode as zipcode,
          a.woonplaats as city,
          a.functie_adres as address_type,
          mc.json as municipality,
          cc.label as country,
          a.adres_buitenland1 as foreign_address_line_1,
          a.adres_buitenland2 as foreign_address_line_2,
          a.adres_buitenland3 as foreign_address_line_3,
          a.bag_id as bag_id,
          a.geo_lat_long[0] as latitude,
          a.geo_lat_long[1] as longitude,
          cd.mobiel as mobile,
          cd.telefoonnummer as phone_number,
          cd.email as email_address
        FROM
          natuurlijk_persoon np
        LEFT JOIN
          adres a
        ON
          np.adres_id = a.id
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = a.landcode
        LEFT JOIN
          municipality_code_v1_view mc
        ON
          mc.dutch_code = a.gemeente_code
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = contactdata_person)

        WHERE
          np.id = gm_id;
      ELSE

        SELECT INTO
          subject
          np.*,
          a.straatnaam as street,
          a.huisnummer as street_number,
          a.huisletter as street_number_letter,
          a.huisnummertoevoeging as street_number_suffix,
          a.postcode as zipcode,
          a.woonplaats as city,
          a.functie_adres as address_type,
          mc.json as municipality,
          cc.label as country,
          a.adres_buitenland1 as foreign_address_line_1,
          a.adres_buitenland2 as foreign_address_line_2,
          a.adres_buitenland3 as foreign_address_line_3,
          a.bag_id as bag_id,
          a.geo_lat_long[0] as latitude,
          a.geo_lat_long[1] as longitude,
          cd.mobiel as mobile,
          cd.telefoonnummer as phone_number,
          cd.email as email_address
        FROM
          gm_natuurlijk_persoon np
        LEFT JOIN
          gm_adres a
        ON
          np.adres_id = a.id
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = a.landcode
        LEFT JOIN
          municipality_code_v1_view mc
        ON
          mc.dutch_code = a.gemeente_code
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = contactdata_person)

        WHERE
          np.gegevens_magazijn_id = gm_id and np.id = b_id;
      END IF;

      s_hstore := hstore(subject);

      SELECT INTO json person_as_v0_json(s_hstore, type);

    ELSIF btype = 'bedrijf' THEN

      IF historic = false
      THEN

        SELECT INTO subject
        b.*,
        let.label as company_type,
        cc.label as country_of_residence,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
        FROM
          bedrijf b
        LEFT JOIN
          legal_entity_type let
        ON
          let.code = b.rechtsvorm
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = b.vestiging_landcode
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = b.id and cd.betrokkene_type = contactdata_company)
        WHERE
          b.id = gm_id;
      ELSE

        SELECT INTO subject
        b.*,
        let.label as company_type,
        cc.label as country_of_residence,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
        FROM
          gm_bedrijf b
        LEFT JOIN
          legal_entity_type let
        ON
          let.code = b.rechtsvorm
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = b.vestiging_landcode
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = b.id and cd.betrokkene_type = contactdata_company)
        WHERE
          b.gegevens_magazijn_id = gm_id and b.id = b_id;

      END IF;

      s_hstore := hstore(subject);

      SELECT INTO json company_as_v0_json(s_hstore, type);
      RETURN;

    END IF;

  END;
  $$;

COMMIT;
