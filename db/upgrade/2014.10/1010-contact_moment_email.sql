BEGIN;

	ALTER TABLE contactmoment_email ADD COLUMN cc TEXT;
	ALTER TABLE contactmoment_email ADD COLUMN bcc TEXT;

COMMIT;
