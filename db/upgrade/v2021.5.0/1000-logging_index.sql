DROP INDEX IF EXISTS logging_custom_object_idx;
CREATE INDEX CONCURRENTLY logging_custom_object_idx ON logging((event_data::json->>'custom_object_uuid'))
    WHERE component = 'custom_object' AND event_data::json->>'custom_object_uuid' IS NOT NULL;
