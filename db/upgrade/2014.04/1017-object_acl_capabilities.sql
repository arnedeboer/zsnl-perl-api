BEGIN;

ALTER TABLE object_acl_entry RENAME COLUMN permission TO capability;
ALTER TABLE object_acl_entry ADD COLUMN verdict TEXT;
ALTER TABLE object_acl_entry ADD CONSTRAINT object_acl_entry_verdict_check CHECK(
    verdict ~ '^(permission|proscription)$'
);

COMMIT;
