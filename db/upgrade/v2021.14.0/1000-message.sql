BEGIN;

ALTER TABLE message
    ADD created TIMESTAMP WITHOUT TIME ZONE;

UPDATE
    message
SET
    created = COALESCE(l.created, l.last_modified)
FROM
    logging l
WHERE
    l.id = message.logging_id;

ALTER TABLE message
    ALTER created SET DEFAULT CURRENT_TIMESTAMP AT TIME ZONE 'UTC';

ALTER TABLE message
    ALTER created SET NOT NULL;

DROP INDEX IF EXISTS message_created_desc_idx;

CREATE INDEX message_created_desc_idx ON message (subject_id, is_archived, created DESC);

COMMIT;

