BEGIN;

    CREATE TABLE zaaktype_standaard_betrokkenen (
        id SERIAL primary key,
        zaaktype_node_id INTEGER REFERENCES zaaktype_node (id) NOT NULL,
        zaak_status_id INTEGER REFERENCES zaaktype_status (id) NOT NULL,
        betrokkene_type TEXT NOT NULL,
        betrokkene_identifier TEXT NOT NULL,
        naam TEXT NOT NULL,
        rol TEXT NOT NULL,
        magic_string_prefix TEXT,
        gemachtigd BOOLEAN DEFAULT FALSE NOT NULL,
        notify BOOLEAN DEFAULT FALSE NOT NULL,
        uuid uuid DEFAULT uuid_generate_v4()
    );

COMMIT;
