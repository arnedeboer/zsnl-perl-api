BEGIN;

-- Deduplicate file UUIDs
WITH cte AS (
    SELECT
        row_number() OVER (PARTITION BY uuid ORDER BY id) AS rno,
        uuid,
        id
    FROM
        file
     WHERE root_file_id IS NULL)
UPDATE
    file
SET
    uuid = uuid_generate_v4 ()
FROM
    cte
WHERE
    cte.id = file.id
    AND cte.rno > 1;

-- But keep the same UUID in a "version chain"
UPDATE file
SET uuid = file2.uuid
FROM file as file2 WHERE file.root_file_id = file2.id AND file.case_id = file2.case_id;

COMMIT;

