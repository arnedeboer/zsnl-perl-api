package Zaaksysteem::Object::Types::File::Lock;

use Moose;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::File::Lock - File lock metadata wrapper

=head1 DESCRIPTION

=cut

use BTTW::Tools;

use Zaaksysteem::Types qw[Timestamp];

=head1 ATTRIBUTES

=head2 date_expires

Stores the L<timestamp|Zaaksysteem::Types/Timestamp> on which the lock
expires.

=cut

has date_expires => (
    is => 'rw',
    isa => Timestamp,
    label => 'Date of lock expiration',
    traits => [qw[OA]],
    required => 1
);

=head2 subject

Stores a reference to a L<C<subject>|Zaaksysteem::Object::Types::Subject>
object which represents the owner of the lock.

=cut

has subject => (
    is => 'rw',
    type => 'subject',
    label => 'Subject',
    traits => [qw[OR]],
    required => 1
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
