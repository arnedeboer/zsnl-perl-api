package Zaaksysteem::Object::ValueType::ObjectRef;

use Moose;
use namespace::autoclean;

with 'Zaaksysteem::Interface::ValueType';

=head1 NAME

Zaaksysteem::Object::ValueType::ObjectRef - Abstracts the notion of a
reference-to-an-object value

=head1 DESCRIPTION

This value type is used to pass around references to objects in the object
infrastructure.

It can be used to retrieve fully inflated objects, or relate objects to one
another.

=head1 METHODS

=head2 name

Returns the static value type string C<object_ref>.

=cut

sub name {
    return 'object_ref'
}

=head2 equal

Implements equality testing for values in C<object_ref> context.

=cut

sub equal {
    my $self = shift;
    my $a = $self->as_object_ref(shift);
    my $b = $self->as_object_ref(shift);

    return unless defined $a && defined $b;
    return unless $a->has_id && $b->has_id;

    return $a->id eq $b->id && $a->type eq $b->type;
}

=head2 not_equal

Implements non-equality testing for values in C<object_ref> context.

=cut

sub not_equal {
    return not shift->equal(@_);
}

=head2 as_object_ref

Returns a L<Zaaksysteem::Object::Reference>-implementing instance which
represents the actual reference value.

=cut

sub as_object_ref {
    my $self = shift;
    my $value = shift;

    return unless ($value->type_name eq 'object_ref');
    return $value->value;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
