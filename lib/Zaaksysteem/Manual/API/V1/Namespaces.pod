=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces - API Namespace listing

=head1 DESCRIPTION

This page lists documentation for all API namespaces.

=head1 NAMESPACES

=over 4

=item L<app|Zaaksysteem::Manual::API::V1::Namespaces::App>

The App API is reserved for use by Zaaksysteem-based apps, and contains
endpoints specifically built for the apps.

=item L<case|Zaaksysteem::Manual::API::V1::Namespaces::Case>

The Case API defines endpoints for
L<case|Zaaksysteem::Manual::API::V1::Types::Case> object instances.

=item L<casetype|Zaaksysteem::Manual::API::V1::Namespaces::Casetype>

The Casetype API defines endpoints for
L<casetype|Zaaksysteem::Manual::API::V1::Types::Casetype> objects.

=item L<controlpanel|Zaaksysteem::Manual::API::V1::Namespaces::Controlpanel>

The Controlpanel API defines endpoints for
L<controlpanel|Zaaksysteem::Manual::API::V1::Types::Controlpanel> objects.

=item L<conversation|Zaaksysteem::Manual::API::V1::Namespaces::Conversation>

The Conversation API defines endpoints for
L<contactmoment|Zaaksysteem::Manual::API::V1::Types::Contactmoment> objects.

=item L<dashboard|Zaaksysteem::Manual::API::V1::Namespaces::Dashboard>

The Dashboard API defines endpoints for a user's settings and widgets on their
dashboard.

=item L<general|Zaaksysteem::Manual::API::V1::Namespaces::General>

The General API is a category of miscellaneous sub-namespaces and endpoints,
such as the instance configuration and static lists of data.

=item L<map|Zaaksysteem::Manual::API::V1::Namespaces::Map>

The Map API defines endpoints for retrieving WebMapService (WMS) layers, which
can be used in conjunction with the location data found in certain objects.

=item L<scheduled_job|Zaaksysteem::Manual::API::V1::Namespaces::ScheduledJob>

The Scheduled Job API defines endpoints for managing
L<scheduled_job|Zaaksysteem::Manual::API::V1::Types::ScheduledJob> objects.

These objects are used in Zaaksysteem automation, and include contexts such as
delayed creation of objects, scheduled e-mail sending, etc.

=item L<search|Zaaksysteem::Manual::API::V1::Namespaces::Search>

The Search API defines endpoints for generic keyword-based queries on
Zaaksysteem objects. It is heavily used by auto-completeable form inputs.

=item L<session|Zaaksysteem::Manual::API::V1::Namespaces::Session>

The Session API defines endpoints for looking up session information about
the current user.

=item L<status|Zaaksysteem::Manual::API::V1::Namespaces::Status>

The Status API contains stub endpoints, suitable for connection testing.
Clients may use any of these endpoints to check if the Zaaksysteem instance
is reachable.

=item L<subject|Zaaksysteem::Manual::API::V1::Namespaces::Subject>

The Subject API contains endpoints for retrieving, updating, creating, and
importing L<subject|Zaaksysteem::Manual::API::V1::Types::Subject>s into
Zaaksysteem.

=item L<subject role|Zaaksysteem::Manual::API::V1::Namespaces::Subject::Role>

The Subject Role API contains endpoints for retrieving the list of configured
L<subject|Zaaksysteem::Manual::API::V1::Types::Subject::Role>s from
Zaaksysteem.

=item L<sysin|Zaaksysteem::Manual::API::V1::Namespaces::Sysin>

The Sysin API contains endpoints for managing system integration profiles on
the Zaaksysteem instance. Examples include integration profiles for StUF,
Overheid.io, and E-mail servers.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
