package Zaaksysteem::Zaken::PlannedCase;
use Zaaksysteem::Moose;

with qw(
    Zaaksysteem::Moose::Role::Schema
);
use List::Util qw(uniq);

=head1 NAME

Zaaksysteem::Zaken::PlannedCase - A model for planned cases

=head1 DESCRIPTION

A model that deals with planned cases

=head1 SYNOPSIS

    package Zaaksysteem::Example;
    use Zaaksysteem::Zaken::PlannedCase;

    my $model = Zaaksysteem::Zaken::PlannedCase->new(
        schema => $schema,
        case   => $case,
    );

    my $planned_cases = $model->get_planned_cases;
    #
    #{
    #    uuid => $job_id,
    #
    #    title    => $casetype->title,
    #    casetype => $casetype->uuid,
    #
    #    date_created => UTC,
    #    next_run     => UTC,
    #
    #    interval_period => 'once', # 'days'
    #    interval_value  => 9,
    #    runs_left       => 1,
    #}
    #

=head1 ATTRIBUTES

=head2 case

A Zaaksysteem case

=cut

has case => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Zaken::ComponentZaak',
    required => 1,
);

has _relation_rs => (
    is       => 'ro',
    isa      => 'DBIx::Class::ResultSet',
    lazy     => 1,
    builder  => '_build_relation_rs',
    init_arg => undef,
);

sub _build_relation_rs {
    my $self = shift;

    return $self->build_resultset(
        'ObjectRelationships',
        {
            type1        => 'job_dependency',
            type2        => 'scheduled_by',
            object1_type => 'scheduled_job'
        }
    );
}

has _casetype_rs => (
    is       => 'ro',
    isa      => 'Zaaksysteem::DB::ResultSet::Zaaktype',
    lazy     => 1,
    builder  => '_build_casetype_rs',
    init_arg => undef,
);

sub _build_casetype_rs {
    my $self = shift;
    return $self->build_resultset('Zaaktype',
        { 'me.active' => 1, 'me.deleted' => undef },
        { prefetch  => 'zaaktype_node_id' },
    );
}

has _planned_casetypes => (
    is       => 'ro',
    isa      => 'Zaaksysteem::DB::ResultSet::Zaaktype',
    lazy     => 1,
    builder  => '_get_planned_casetypes',
    init_arg => undef,
);

sub _get_planned_casetypes {
    my $self = shift;

    my $jobs = $self->_scheduled_jobs;
    my @uuids;
    foreach my $rel (@{$jobs}) {
        push(@uuids, $rel->get_column('object2_uuid'));
    }

    @uuids = uniq @uuids;
    return $self->_casetype_rs->search_rs(
        { 'me.uuid' => { -in => \@uuids } }
    );
}


has _scheduled_jobs => (
    is       => 'ro',
    isa      => 'ArrayRef',
    lazy     => 1,
    builder  => '_build_get_scheduled_jobs',
    init_arg => undef,
);

sub _build_get_scheduled_jobs {
    my $self = shift;

    my $jobs = $self->_relation_rs->search_rs(
        {
            object2_uuid => $self->case->get_column('uuid'),
            object2_type => 'case',
            object1_type => 'scheduled_job',
        }
    )->get_column('object1_uuid');

    my $casetype_jobs = $self->_relation_rs->search_rs(
        {
            object1_uuid => { -in => $jobs->as_query },
            object2_type => 'casetype',
        }
    );

    my @jobs = $casetype_jobs->all;
    return \@jobs;

}

has _scheduled_jobs_rs => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Backend::Object::Data::ResultSet',
    lazy     => 1,
    builder  => '_build_scheduled_jobs_rs',
    init_arg => undef,
);

sub _build_scheduled_jobs_rs {
    my $self = shift;

    return $self->build_resultset('ObjectData',
        { object_class => 'scheduled_job' });
}

has _scheduled_jobs_objects => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    builder  => '_get_scheduled_jobs_objects',
    init_arg => undef,
);

sub _get_scheduled_jobs_objects {
    my $self = shift;

    my @uuids
        = map { $_->get_column('object1_uuid') } @{ $self->_scheduled_jobs };

    my $objects
        = $self->_scheduled_jobs_rs->search_rs({ uuid => { -in => \@uuids } });

    my %jobs;
    while (my $j = $objects->next) {
        $jobs{ $j->uuid } = $j;

    }
    return \%jobs;

}

=head2 get_planned_cases

Get the planned cases. Returns a ArrayRef filled with HashRef's which look a little like this:

    {
        uuid => $job_id,

        title    => $casetype->title,
        casetype => $casetype->uuid,

        date_created => UTC,
        next_run     => UTC,

        interval_period => 'once',    # 'days'
        interval_value  => 9,
        runs_left       => 1,
        job             => "CreateCase",
    }

=cut

has _planned_cases => (
    is       => 'ro',
    isa      => 'ArrayRef[HashRef]',
    lazy     => 1,
    builder  => '_get_planned_cases',
    init_arg => undef,
    reader   => 'get_planned_cases',
);

sub _get_planned_cases {
    my $self = shift;

    my $jobs = $self->_scheduled_jobs;
    return [] unless @{$jobs};

    my $job_objects = $self->_scheduled_jobs_objects;
    my $casetypes   = $self->_planned_casetypes;

    my %map;
    while (my $ct = $casetypes->next) {
        $map{ $ct->uuid } = $ct;
    }

    my @scheduled;
    foreach my $job (@{$jobs}) {

        my $job_id     = $job->get_column('object1_uuid');
        my $job_object = $job_objects->{$job_id};
        my $casetype   = $map{ $job->get_column('object2_uuid') };
        my $properties = $job_object->properties->{values};

        push(
            @scheduled,
            {
                uuid => $job_id,

                title    => $casetype->title,
                casetype => $casetype->uuid,

                date_created => $properties->{date_created}{value},
                next_run     => $properties->{next_run}{value},

                interval_period => $properties->{interval_period}{value},
                interval_value  => int($properties->{interval_value}{value}),
                runs_left       => int($properties->{runs_left}{value}),
                job             => "CreateCase",
            }
        );
    }
    return \@scheduled;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
