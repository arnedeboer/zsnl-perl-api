package Zaaksysteem::StUF::ZKN::0102::Model;
use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::StUF::ZKN::0102::Model - A StUF-ZKN model for version 1.2

=head1 DESCRIPTION

This model implements a subset of StUF-ZKN version 1.2.

The following SOAP actions are supported:

  http://www.stufstandaarden.nl/koppelvlak/zds0120/geefLijstZaakdocumenten_ZakLv01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/geefZaakdocumentLezen_EdcLv01

The standard defines the following SOAP actions:

  http://www.egem.nl/StUF/StUF0301/Bv01
  http://www.egem.nl/StUF/StUF0301/Bv03
  http://www.egem.nl/StUF/StUF0301/Fo01
  http://www.egem.nl/StUF/StUF0301/Fo01
  http://www.egem.nl/StUF/StUF0301/Fo03
  http://www.egem.nl/StUF/StUF0301/Fo03

  http://www.stufstandaarden.nl/koppelvlak/zds0120/actualiseerZaakstatus_ZakLk01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/cancelCheckout_Di02
  http://www.stufstandaarden.nl/koppelvlak/zds0120/creeerZaak_ZakLk01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/geefBesluitdetails_BslLv01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/geefLijstBesluiten_ZakLv01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/geefLijstZaakdocumenten_ZakLv01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/geefZaakdetails_ZakLv01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/geefZaakdocumentLezen_EdcLv01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/geefZaakdocumentbewerken_Di02
  http://www.stufstandaarden.nl/koppelvlak/zds0120/geefZaakstatus_ZakLv01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/genereerBesluitIdentificatie_Di02
  http://www.stufstandaarden.nl/koppelvlak/zds0120/genereerDocumentIdentificatie_Di02
  http://www.stufstandaarden.nl/koppelvlak/zds0120/genereerZaakIdentificatie_Di02
  http://www.stufstandaarden.nl/koppelvlak/zds0120/maakZaakdocument_EdcLk01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/ontkoppelZaakdocument_Di02
  http://www.stufstandaarden.nl/koppelvlak/zds0120/overdragenZaak_Di01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/overdragenZaak_Du01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/updateBesluit_BslLk01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/updateZaak_ZakLk01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/updateZaakdocument_Di02
  http://www.stufstandaarden.nl/koppelvlak/zds0120/voegBesluitToe_Di01
  http://www.stufstandaarden.nl/koppelvlak/zds0120/voegZaakdocumentToe_EdcLk01

=head1 SYNOPSIS

=cut

extends 'Zaaksysteem::StUF::ZKN';

use BTTW::Tools;

=head1 ATTRIBUTES

=head2 major_version

Defaults to 1

=head2 minor_version

Defaults to 2

=cut

has '+major_version' => ( default => 1 );
has '+minor_version' => ( default => 2 );

=head2 soap_actions

The supported SOAP Actions of this module

=cut

# The dispatch table makes sure that the soap action gets mapped to a
# function and serves as a source for the soap_actions attribute adding
# a new function will automaticly add a new soap action.
my $NS = 'http://www.stufstandaarden.nl/koppelvlak/zds0120';
my %DISPATCH_TABLE = (
    "$NS/geefLijstZaakdocumenten_ZakLv01" => '_process_geefLijstZaakdocumenten_ZakLv01',
    "$NS/geefZaakdocumentLezen_EdcLv01"   => '_process_geefZaakdocumentLezen_EdcLv01',
);

has '+soap_actions' => (
    default => sub {
        return [ sort keys %DISPATCH_TABLE];
    }
);


sub process_action {
    my ($self, $action, $xml) = @_;

    if (!$self->supports_soap_action($action)) {
        throw("Unsupported SOAP-Action '$action'");
    }

    my $function = $DISPATCH_TABLE{$action};
    return $self->$function($xml);
}

sub _process_geefLijstZaakdocumenten_ZakLv01 {
    my $self = shift;

    ...;
}

sub _process_geefZaakdocumentLezen_EdcLv01 {
    my $self = shift;

    ...;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
