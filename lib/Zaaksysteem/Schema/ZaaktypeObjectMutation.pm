use utf8;
package Zaaksysteem::Schema::ZaaktypeObjectMutation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaaktypeObjectMutation

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaaktype_object_mutation>

=cut

__PACKAGE__->table("zaaktype_object_mutation");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_object_mutation_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 zaaktype_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 attribute_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 mutation_type

  data_type: 'text'
  is_nullable: 0

=head2 object_title

  data_type: 'text'
  is_nullable: 1

=head2 phase_transition

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 system_attribute_changes

  data_type: 'jsonb'
  default_value: '{}'
  is_nullable: 0

=head2 attribute_mapping

  data_type: 'jsonb'
  default_value: '{}'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_object_mutation_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "zaaktype_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "attribute_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "mutation_type",
  { data_type => "text", is_nullable => 0 },
  "object_title",
  { data_type => "text", is_nullable => 1 },
  "phase_transition",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "system_attribute_changes",
  { data_type => "jsonb", default_value => "{}", is_nullable => 0 },
  "attribute_mapping",
  { data_type => "jsonb", default_value => "{}", is_nullable => 0 },
);

=head1 RELATIONS

=head2 attribute_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "attribute_uuid",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { uuid => "attribute_uuid" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);

=head2 zaaktype_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaaktype_status_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-01-07 10:32:57
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:pbCj3C1/gaKuJwlp9s8JMQ


__PACKAGE__->set_primary_key("id");

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeObjectMutation');

my @json_fields = qw(attribute_mapping system_attribute_changes);
foreach (@json_fields) {
    __PACKAGE__->inflate_column(
        $_,
        {
            inflate => sub { JSON::XS->new->decode(shift // '{}') },
            deflate => sub { JSON::XS->new->encode(shift // '{}') },
        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
