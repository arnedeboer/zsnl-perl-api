package Zaaksysteem::API::v1::Serializer::Reader::Message;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Message -

=head1 DESCRIPTION

=head1 METHODS

=head2 class

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::API::v1::Message' }

=head2 read

Implements interface required by

L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub read {
    my ($class, $serializer, $message) = @_;

    return {
        type => 'message',
        reference => undef,
        instance => $message->field_hash
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
