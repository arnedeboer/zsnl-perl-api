package Zaaksysteem::Geo::BAG::Object;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Geo::BAG::Object - BAG Object (as returned by the BAG API)

=head1 SYNOPSIS

    my $bag_object = Zaaksysteem::Geo::BAG::Object->new(bag_object => $object);
    my $woonplaats_naam = $bag_object->woonplaats_as_string;

=head1 DESCRIPTION

The BAG API returns plain JSON objects (= hash refs). This class wraps those
objects to make some convenience methods available for nice displaying/
structuring of the data.

=head1 ATTRIBUTES

=head2 bag_object

A response from the PDOK location service. Looks like this for a specific
address:

    {
        "adresseerbaarobject_id": "0984010000315711",
        "bron": "BAG",
        "buurtcode": "BU09840301",
        "buurtnaam": "Merselo",
        "centroide_ll": "POINT(5.90448436 51.53270836)",
        "centroide_rd": "POINT(190892.329 393874.927)",
        "gekoppeld_perceel": [ "VRY00-X-305" ],
        "gemeentecode": "0984",
        "gemeentenaam": "Venray",
        "huis_nlt": "1",
        "huisnummer": 1,
        "id": "adr-8f7cc0ea12890c9b92ca5a10efb0648f",
        "identificatie": "0984010000315711-0984200000026420",
        "nummeraanduiding_id": "0984200000026420",
        "openbareruimte_id": "0984300000000742",
        "openbareruimtetype": "Weg",
        "postcode": "5815CK",
        "provincieafkorting": "LB",
        "provinciecode": "PV31",
        "provincienaam": "Limburg",
        "rdf_seealso": "http://bag.basisregistraties.overheid.nl/bag/id/nummeraanduiding/0984200000026420",
        "score": 17.069046
        "straatnaam_verkort": "Testrik",
        "straatnaam": "Testrik",
        "type": "adres",
        "waterschapscode": "54",
        "waterschapsnaam": "Waterschap Limburg",
        "weergavenaam": "Testrik 1, 5815CK Merselo",
        "wijkcode": "WK098403",
        "wijknaam": "Merselo",
        "woonplaatscode": "2331",
        "woonplaatsnaam": "Merselo",
    }

And like this for a street:

    {
        "bron": "BAG/NWB",
        "centroide_ll": "POINT(5.90402403 51.53279022)",
        "centroide_rd": "POINT(190860.324 393883.807)",
        "gemeentecode": "0984",
        "gemeentenaam": "Venray",
        "id": "weg-02ca097ac53f1fd285c768f7a474fbb5",
        "identificatie": "0984300000000742",
        "openbareruimte_id": "0984300000000742",
        "openbareruimtetype": "Weg",
        "provincieafkorting": "LB",
        "provinciecode": "PV31",
        "provincienaam": "Limburg",
        "rdf_seealso": "http://bag.basisregistraties.overheid.nl/bag/id/openbare-ruimte/0984300000000742",
        "score": 26.461073
        "straatnaam_verkort": "Testrik",
        "straatnaam": "Testrik",
        "type": "weg",
        "weergavenaam": "Testrik, Merselo",
        "woonplaatscode": "2331",
        "woonplaatsnaam": "Merselo",
    }

=cut

has bag_object => (
    is => 'ro',
    isa => 'HashRef',
    required => 1,
);

=head1 METHODS

=head2 address_data

Returns address data, in parts.

=cut

sub address_data {
    my $self = shift;
    my $bag_object = $self->bag_object;

    return if $bag_object->{type} ne 'adres';

    return {
        identification => $bag_object->{nummeraanduiding_id},
        woonplaats => $bag_object->{woonplaatsnaam},
        straat => $bag_object->{straatnaam},
        postcode => $bag_object->{postcode},
        huisnummer => $bag_object->{huisnummer},
        huisletter => $bag_object->{huisletter},
        huisnummertoevoeging => $bag_object->{huisnummertoevoeging},
        gps_lat_lon => $self->coordinates,
    };
}

=head2 woonplaats_as_string

Returns the "woonplaats" name.

=cut

sub woonplaats_as_string {
    my $self = shift;
    return $self->bag_object->{woonplaatsnaam};
}

=head2 openbareruimte_as_string

Returns the street name, prefixed with the town name, in the form
"Street, Town".

=cut

sub openbareruimte_as_string {
    my $self = shift;

    return unless $self->bag_object->{type} eq 'weg';

    return $self->bag_object->{weergavenaam};
}

=head2 nummeraanduiding_id

Retrieve the old-style "nummeraanduiding-id" in the form "nummeraanduiding-XXXX", where
XXX is the BAG-Nummeraanduiding identification for this object.

=cut

sub nummeraanduiding_id {
    my $self = shift;

    return unless $self->bag_object->{type} eq 'adres';

    return sprintf(
        'nummeraanduiding-%016d',
        $self->bag_object->{nummeraanduiding_id}
    );
}

=head2 nummeraanduiding_as_string

Returns the address in the form "Town - Street 123a-hs".

=cut

sub nummeraanduiding_as_string {
    my $self = shift;
    
    return unless $self->bag_object->{type} eq 'adres';
    return $self->bag_object->{weergavenaam};
}

=head2 get_identifiers

Returns a hash reference containing all the BAG identifiers, suitable for creating
a C<ZaakBag> row.

=cut

sub get_identifiers {
    my $self = shift;
    my $bag_object = $self->bag_object;

    my %rv = (
        bag_openbareruimte_id => $bag_object->{openbareruimte_id},
    );

    if (exists $bag_object->{nummeraanduiding_id}) {
        $rv{bag_nummeraanduiding_id} = $bag_object->{nummeraanduiding_id};
    }
    if (exists $bag_object->{adresseerbaarobject_id}) {
        if ($self->is_verblijfsobject) {
            $rv{bag_verblijfsobject_id} = $bag_object->{adresseerbaarobject_id};
        } elsif($self->is_ligplaats) {
            $rv{bag_ligplaats_id} = $bag_object->{adresseerbaarobject_id};
        } elsif($self->is_standplaats) {            
            $rv{bag_standplaats_id} = $bag_object->{adresseerbaarobject_id};
        } else {
            $self->log->debug("Unknown object type ($bag_object->{adresseerbaarobject_id})");
        }
    }

    if (my $coords = $self->coordinates) {
        $rv{bag_coordinates_wsg} = sprintf("(%s)", $coords);
    }

    return \%rv;
}

=head2 to_attribute_value

Returns the value as stored in Case object attributes.

=cut

sub to_attribute_value {
    my $self = shift;
    my $bag_type = shift;

    my $address_data;
    my $human_identifier = $self->bag_object->{weergavenaam};
    my $bag_id;

    if ($bag_type eq 'openbareruimte') {
        return if not exists $self->bag_object->{openbareruimte_id};
        
        $bag_id = $self->bag_object->{openbareruimte_id};
        $address_data = {
            identification => $self->bag_object->{openbareruimte_id},
            straat => $self->bag_object->{straatnaam},
            gps_lat_lon => $self->coordinates,
        };
    } elsif ($bag_type eq 'nummeraanduiding') {
        return if not exists $self->bag_object->{nummeraanduiding_id};
        
        $bag_id = $self->bag_object->{nummeraanduiding_id};
        $address_data = $self->address_data;
    } elsif ($bag_type eq 'verblijfsobject' and $self->is_verblijfsobject) {
        return if not exists $self->bag_object->{adresseerbaarobject_id};
        
        $bag_id = $self->bag_object->{adresseerbaarobject_id};
        $address_data = {
            identification => $self->bag_object->{adresseerbaarobject_id},
            surface_area => $self->bag_object->{wfs_data}{oppervlakte},
            year_of_construction => $self->bag_object->{wfs_data}{bouwjaar},
            gebruiksdoel => $self->bag_object->{wfs_data}{gebruiksdoel},
        };
    } elsif ($bag_type eq 'ligplaats' and $self->is_ligplaats) {
        return if not exists $self->bag_object->{adresseerbaarobject_id};

        $bag_id = $self->bag_object->{adresseerbaarobject_id};
        $address_data = {
            identification => $self->bag_object->{adresseerbaarobject_id}
        };
    } elsif ($bag_type eq 'standplaats' and $self->is_standplaats) {
        return if not exists $self->bag_object->{adresseerbaarobject_id};

        $bag_id = $self->bag_object->{adresseerbaarobject_id};
        $address_data = {
            identification => $self->bag_object->{adresseerbaarobject_id}
        };
    } else {
        return;
    }

    return {
        bag_id           => join('-', $bag_type, $bag_id),
        human_identifier => $human_identifier,
        address_data     => $address_data, 
    };
}

=head2 to_string

Returns a human-readable form of the address represented by the BAG object.

=cut

sub to_string {
    my $self = shift;

    return $self->bag_object->{weergavenaam};
}

=head2 coordinates

Return the coordinates of the object

=cut

sub coordinates {
    my $self = shift;

    return if not defined $self->bag_object->{centroide_ll};

    my ($lon, $lat) = ($self->bag_object->{centroide_ll} =~ /^POINT\(([0-9]+.[0-9]+) ([0-9]+.[0-9]+)\)$/);

    return sprintf('%f,%f', $lat, $lon);
}

=head2 to_maps_result

Returns the value in a way the "plugins/maps" API endpoint understands.

=cut

sub to_maps_result {
    my $self = shift;

    my ($latitude, $longitude) = split(/,/, $self->coordinates);

    return {
       country  => $self->bag_object->{land},
       city     => $self->bag_object->{woonplaatsnaam},
       street   => $self->bag_object->{straatnaam},
       zipcode  => $self->bag_object->{postcode},
       province => $self->bag_object->{provincienaam},
       coordinates => {
           lat => $latitude,
           lng => $longitude,
       },
       identification => $self->bag_object->{weergavenaam},
       address => $self->bag_object->{weergavenaam},
    }
}

# From the "Systeembeschrijving van de Basisregistratie Adressen en Gebouwen":
my $BAG_CODE_VERBLIJFSOBJECT = '01';
my $BAG_CODE_LIGPLAATS = '02';
my $BAG_CODE_STANDPLAATS = '03';

sub is_verblijfsobject {
    my $self = shift;
    return unless exists $self->bag_object->{adresseerbaarobject_id};
    return substr($self->bag_object->{adresseerbaarobject_id}, 4, 2) eq $BAG_CODE_VERBLIJFSOBJECT;
}

sub is_ligplaats {
    my $self = shift;
    return unless exists $self->bag_object->{adresseerbaarobject_id};
    return substr($self->bag_object->{adresseerbaarobject_id}, 4, 2) eq $BAG_CODE_LIGPLAATS;
}

sub is_standplaats {
    my $self = shift;
    return unless exists $self->bag_object->{adresseerbaarobject_id};
    return substr($self->bag_object->{adresseerbaarobject_id}, 4, 2) eq $BAG_CODE_STANDPLAATS;
}

=head2 TO_JSON

JSON representation, for the "JSONlegacy" view used by ObjectSearch.

=cut

sub TO_JSON {
    my $self = shift;

    my $bag_object = $self->bag_object;

    if (exists $bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}) {
        my $nummeraanduiding_id = $self->nummeraanduiding_id;

        my $huisnummer = _build_huisnummer($bag_object->{woonplaats}{openbareruimte}{nummeraanduiding});

        return {
            id => $nummeraanduiding_id,
            object_type => 'bag_address',
            label  => $self->nummeraanduiding_as_string,
            object => {
                id          => $nummeraanduiding_id,
                type        => 'address',
                number      => $huisnummer,
                street      => $bag_object->{straatnaam},
                postal_code => $bag_object->{postcode},
                city        => $bag_object->{plaats},
            },
        };
    }
    else {
        my $openbareruimte_id = sprintf(
            'openbareruimte-%s',
            $self->bag_object->{woonplaats}{openbareruimte}{id}
        );

        return {
            id          => $openbareruimte_id,
            object_type => 'bag_street',
            label       => $self->openbareruimte_as_string,
            object      => {
                id         => $openbareruimte_id,
                type       => 'street',
                streetname => $bag_object->{straatnaam},
                city       => $bag_object->{plaats},
            }
        }
    }
}

=head2 _build_huisnummer

Build up the "full" house number from parts (huisnummer, huisletter, huisnummer_toevoeging).

=cut

sub _build_huisnummer {
    my $nummeraanduiding = shift;

    my $huisnummer = $nummeraanduiding->{huisnummer};
    my $huisletter = $nummeraanduiding->{huisletter};
    my $huisnummer_toevoeging = $nummeraanduiding->{huisnummer_toevoeging};

    my $hn = $huisnummer;
    $hn .= $huisletter
        if defined $huisletter && length($huisletter);
    $hn .= "-$huisnummer_toevoeging"
        if defined $huisnummer_toevoeging && length($huisnummer_toevoeging);

    return $hn;
}

=head2 _extract_common_fields

Extracts the common fields from a part of the BAG object.

The common fields are:

=over

=item * start_date

=item * end_date

=item * document_date

=item * document_number

=item * under_investigation

=back

=cut

{
    my %COMMON_FIELDS = (
        begindatum     => 'start_date',
        einddatum      => 'end_date',
        documentdatum  => 'document_date',
        documentnummer => 'document_number',
        in_onderzoek   => 'under_investigation',
        id             => 'identification',
    );

    sub _extract_common_fields {
        my $object = shift;

        my %rv;
        for my $key (keys %COMMON_FIELDS) {
            my $return_key = $COMMON_FIELDS{$key};
            $rv{$return_key} = $object->{$key};
        }

        return %rv;
    }
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
