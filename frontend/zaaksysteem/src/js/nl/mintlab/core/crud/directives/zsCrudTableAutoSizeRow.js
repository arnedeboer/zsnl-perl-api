// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsCrudTableAutoSizeRow', [
    function () {
      return {
        require: ['^zsCrudTableAutoSize', 'zsCrudTableAutoSizeRow'],
        controller: [
          function () {
            var parent = null,
              cells = [];
            return {
              register: function (cell) {
                cells.push(cell);
                if (parent) {
                  parent.invalidate();
                }
              },
              unregister: function (cell) {
                var index = _.indexOf(cells, cell);
                if (index !== -1) {
                  cells.splice(index, 1);
                }
                if (parent) {
                  parent.invalidate();
                }
              },
              setParent: function (p) {
                parent = p;
                if (parent) {
                  parent.invalidate();
                }
              },
            };
          },
        ],
        link: function (scope, element, attrs, controllers) {
          var zsCrudTableAutoSize = controllers[0],
            zsCrudTableAutoSizeRow = controllers[1];

          zsCrudTableAutoSizeRow.setParent(zsCrudTableAutoSize);

          zsCrudTableAutoSize.register(element[0]);

          scope.$on('destroy', function () {
            zsCrudTableAutoSize.unregister(element[0]);
          });
        },
      };
    },
  ]);
})();
