// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin.objecttype')
    .directive('zsObjectTypeCasetypeFormField', [
      function () {
        return {
          controller: [
            function () {
              var ctrl = this;

              ctrl.transform = function ($object) {
                return {
                  // leave .values here for backward compatibility
                  related_object_title:
                    $object.values['casetype.name'] || $object.label,
                  related_object: null,
                  related_object_id: $object.id,
                  related_object_type: 'casetype',
                };
              };

              return ctrl;
            },
          ],
          controllerAs: 'objectTypeCasetypeFormField',
        };
      },
    ]);
})();
