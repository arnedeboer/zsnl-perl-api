#!/usr/bin/perl

use strict;
use warnings;

use Moose;
use Data::Dumper;

use Text::CSV;
use Unicode::String;
use utf8;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Catalyst qw/
    ConfigLoader
/;

use Catalyst::Log;
use Catalyst::Model::DBIC::Schema;
use Zaaksysteem::Zaken;

### Start logging
my $log         = Catalyst::Log->new();


error("USAGE: $0 [dsn] [user] [password] [commit]") unless @ARGV && scalar @ARGV >= 3;

my ($dsn, $user, $password, $commit) = @ARGV;
my $dbic = database($dsn, $user, $password);


$dbic->txn_do(sub {
    eval {
        fill_new_table($dbic);
        die "rollback" unless $commit;
    };

    if ($@) {
        warn('Error: ' . $@);
        die("Import error: " . $@);
    }
});



sub database {
    my ($dsn, $user, $password) = @_;

    Catalyst::Model::DBIC::Schema->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => {
            dsn             => $dsn,
            pg_enable_utf8  => 1,
            user            => $user,
            password        => $password
        }
    );

    my $db = Catalyst::Model::DBIC::Schema->new();
    my $dbic = $db->schema;

    $log->info('Connection to DB established');
    return $dbic;
}


sub error {
    my ($msg) = @_;

    $log->error($msg);
    $log->_flush;

    exit;
}


sub fill_new_table {
    my ($dbic) = @_;
#INSERT INTO zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id, value)

    ### Correct bewaartermijnen
    my $resultaten = $dbic->resultset("ZaaktypeResultaten")->search({
        'bewaartermijn' => {
            'not in'    => [62, 365, 184, 730, 1095, 1460, 1825, 2190, 2555,
            2920, 3285, 3650, 4015, 4380, 4745, 5110, 5475, 7300, 10950,
            14600, 40150, 99999
            ],
        },
    },
    {
        order_by    => 'id'
    }
    );

    while (my $resultaat = $resultaten->next) {
        next unless $resultaat->bewaartermijn;
        my $newtermijn = 99999;

        if ($resultaat->bewaartermijn ne '999') {
            $newtermijn = ($resultaat->bewaartermijn * 365);
        }

        print 'Correct bewaartermijn: '
            . $resultaat->bewaartermijn . " TO: "
            . $newtermijn . "\n";

        $resultaat->bewaartermijn($newtermijn);
        $resultaat->update;
    }



    print "querying for values\n";

    my $zaken = $dbic->resultset("Zaak")->search_extended({
#        'me.zaaktype_id' => {
#            '-in'    => [5,6,2,3,10,4,12],
#        },
    },
    {
        order_by    => 'me.id'
    }
    );

    my $total = $zaken->count;
    my $count = 0;
    my $percentage = 0;
    while (my $zaak = $zaken->next) {
        $zaak->set_vernietigingsdatum;
        $zaak->update;
        $count++;
        my $new_percentage = int(100* $count / $total);
        if($new_percentage ne $percentage) {
            print $new_percentage . "\%";
            print "     Zaak : " . $zaak->id;
            print "          Vernietigingsdatum: " . $zaak->afhandeldatum->dmy . "\n"
            if $zaak->afhandeldatum;
            $percentage = $new_percentage;
        }
    }
}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
