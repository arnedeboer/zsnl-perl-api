#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Moose::Util::TypeConstraints qw(find_type_constraint);
use Zaaksysteem::Types::MappedString;

my $ORIGINAL = 'Origineel';
my $MAPPED   = 'Gemapt';

my $ms = Zaaksysteem::Types::MappedString->new(
    original => $ORIGINAL,
    mapped   => $MAPPED,
);

is("$ms", $MAPPED, 'Stringification overload');

my $type = find_type_constraint('Str');
my $coerced = $type->coerce($ms);

is($coerced, $MAPPED, 'Moose coercion to string');

is($ms->mapped,   $MAPPED,   'Mapped value');
is($ms->original, $ORIGINAL, 'Original value');

is_deeply($ms->TO_JSON, {mapped => $MAPPED, original => $ORIGINAL}, 'JSON value');

zs_done_testing();
