// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const fetch = require('node-fetch');
const bucketHost = process.env.BUCKET_HOST;
const hostDict = new Map();

setInterval(() => {
  hostDict.clear();
}, 10 * 60 * 1000);

module.exports = (nonce, unsafe, reqHost) => {
  const whitelistMain = [
    'https://zaaksysteem.nl/',
    'https://siteimproveanalytics.com',
    'https://www.googletagmanager.com',
    'https://www.gstatic.com/recaptcha/',
    'https://www.google.com/recaptcha/api.js',
    'https://*.bus.koppel.app',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const whitelistStyles = [bucketHost].join(' ');

  const whitelistImages = [
    'https://geodata.nationaalgeoregister.nl/',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const whitelistFrames = [
    'https://*.bus.koppel.app',
    'https://login.live.com/',
    'https://*.officeapps.live.com/',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const buildCspHeader = (whitelistDomains) =>
    [
      `default-src 'self' ${whitelistMain} ${whitelistDomains}`,
      `style-src 'self' ${
        unsafe ? "'unsafe-inline'" : `'nonce-${nonce}'`
      } ${whitelistStyles} ${whitelistDomains}`,
      `frame-src 'self' ${whitelistFrames} ${whitelistDomains}`,
      `img-src 'self' blob: data: ${whitelistImages} ${whitelistDomains}`,
      "object-src 'none'",
      "frame-ancestors 'self'",
      "require-trusted-types-for 'script'",
      "connect-src 'self' data: *",
      "base-uri 'self'",
    ].join('; ');

  const cspReq =
    hostDict.get(reqHost) ||
    fetch('https://' + reqHost + '/csp')
      .then((rs) => rs.json())
      .then((res) =>
        res.csp_hosts
          .filter((hostname) => hostname !== '*')
          .map((hostname) => 'https://' + hostname)
          .join(' ')
      )
      .catch(() => '');

  hostDict.set(reqHost, cspReq);

  return cspReq.then(buildCspHeader);
};
