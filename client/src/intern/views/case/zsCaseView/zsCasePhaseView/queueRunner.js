// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import values from 'lodash/values';
import find from 'lodash/find';
import includes from 'lodash/includes';
import each from 'lodash/each';
import merge from 'lodash/merge';
import isUndefined from 'lodash/isUndefined';
import omitBy from 'lodash/omitBy';
import pickBy from 'lodash/pickBy';

const TIMEOUT = 5000;

export default (options) => {
  let { $http, $q, $timeout, opts, ids } = options,
    deferred = {},
    pending = [];

  opts = omitBy(
    merge({}, opts, {
      params: {
        rows_per_page: 100,
      },
    }),
    isUndefined
  );

  let retry = () => {
    return $http(opts).success((data) => {
      each(deferred, (d, reference) => {
        let job = find(data.result.instance.rows, { reference });

        if (
          includes(pending, d) &&
          (!job ||
            includes(['failed', 'finished', 'cancelled'], job.instance.status))
        ) {
          if (job && job.instance.status === 'failed') {
            d.reject(job);
          } else {
            d.resolve(job);
          }
          pending = pending.filter((pendingDeferred) => pendingDeferred !== d);
        }
      });

      if (pending.length) {
        $timeout(retry, TIMEOUT);
      }
    });
  };

  if (ids) {
    deferred = mapValues(keyBy(ids), () => $q.defer());
    $timeout(retry, TIMEOUT);

    pending = values(deferred);

    return $q.when(pending.map((def) => def.promise));
  }

  return $http(opts).then((response) => {
    deferred = mapValues(
      pickBy(keyBy(response.data.result.instance.rows, 'reference'), (job) =>
        includes(['failed', 'finished', 'cancelled'], job.instance.status)
      ),
      () => $q.defer()
    );

    $timeout(retry, TIMEOUT);

    pending = values(deferred);

    return pending.map((d) => d.promise);
  });
};
