// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import ZaaksysteemException from './ZaaksysteemException';

describe('Zaaksysteem Exception testen', () => {
  const err = new ZaaksysteemException('foo/bar', 'This is an exception');

  test('ZaaksysteemException.type', () => {
    expect(err.type).toBe('foo/bar');
  });
  test('ZaaksysteemException.message', () => {
    expect(err.message).toBe('This is an exception');
  });

  test('ZaaksysteemException.toString', () => {
    expect(err.toString()).toBe(`${err.type}: ${err.message}`);
  });
});
