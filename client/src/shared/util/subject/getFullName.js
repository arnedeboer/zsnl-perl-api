// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import isEmpty from '../../vorm/util/isEmpty';

export default function getFullName(subject) {
  const { instance } = subject;

  switch (instance.subject_type) {
    case 'person':
    case 'employee': {
		const { first_names, surname } = instance.subject.instance; // eslint-disable-line

		if (isEmpty(first_names)) { // eslint-disable-line
        return surname;
      }

		return `${first_names} ${surname}`; // eslint-disable-line
    }
    default:
      return instance.display_name;
  }
}
