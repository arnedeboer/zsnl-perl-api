import {
    openPageAs
} from './../../../../../functions/common/navigate';
import {
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';
import {
    getClosedValue
} from './../../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const contactchannels = [
    {
        channelOfContact: 'behandelaar',
        caseNumber: 164
    },
    {
        channelOfContact: 'balie',
        caseNumber: 165
    },
    {
        channelOfContact: 'telefoon',
        caseNumber: 166
    },
    {
        channelOfContact: 'post',
        caseNumber: 167
    },
    {
        channelOfContact: 'email',
        caseNumber: 168
    },
    {
        channelOfContact: 'webformulier',
        caseNumber: 169
    },
    {
        channelOfContact: 'sociale media',
        caseNumber: 170
    }
];

contactchannels.forEach(contactchannel => {
    const { channelOfContact, caseNumber } = contactchannel;

    describe(`when opening case ${caseNumber} with contactchannel ${channelOfContact}`, () => {
        beforeAll(() => {
            openPageAs('admin', caseNumber);
            openPhase('1');
            choice.$('[value="Ja"]').click();
        });

        contactchannels.forEach(contactchannelAttribute => {
            const attributeName = contactchannelAttribute.channelOfContact;
            const dataName = attributeName.replace(' ', '_');
            const state = channelOfContact === attributeName ? 'True' : 'False';

            it(`the attribute for ${attributeName} should be ${state}`, () => {
                expect(getClosedValue($(`[data-name="contactkanaal_${dataName}"]`))).toEqual(state);
            });
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
