import waitForElement from './../common/waitForElement';

export const enterBasicAttribute = ( attribute, value ) => {
    $(`[name="${attribute}"]`).clear().sendKeys(value);
};

export const enterCasetypeName = ( name ) => {
    enterBasicAttribute('node.titel', name);
};

export const enterIdentification = ( id ) => {
    enterBasicAttribute('node.code', id);
};

export const enterPrincipleNational = ( principle ) => {
    enterBasicAttribute('definitie.grondslag', principle);
};

export const enterTermNational = ( term ) => {
    enterBasicAttribute('definitie.afhandeltermijn', term);
};

export const enterTermService = ( term ) => {
    enterBasicAttribute('definitie.servicenorm', term);
};

export const goTo = ( tabName ) => {
    const tabs = $$('.voortgang a');

    tabs
        .filter(tab =>
            tab.getText().then(text =>
                text === tabName
            )
        )
        .click();
};

export const publishCasetype = () => {
    goTo('afronden');
    waitForElement('[value="basisattributen"]');
    $('[value="basisattributen"]').click();
    $('[value="Publiceren"]').click();

};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
